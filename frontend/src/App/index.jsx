import { useEffect, useState } from 'react';
import { Chat } from './Chat';
import { Form } from './Form';

const socket = new WebSocket('ws://localhost:8080/ws');

export const App = () => {
  const [messages, setMessages] = useState([]);

  useEffect(() => {
    return () => {
      socket.close(1000, 'Component unmounted');
    };
  }, []);

  socket.onmessage = (event) => {
    setMessages((prev) => [event.data, ...prev]);
  };

  socket.onclose = (event) => console.log(`Соединение закрыто ${event.code}`);

  return (
    <div className="container">
      <h1>WebSocket chat</h1>

      <Form socket={socket} />

      <Chat messages={messages} />
    </div>
  );
};
