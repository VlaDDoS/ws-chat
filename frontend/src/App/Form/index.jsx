import { useState } from 'react';

export const Form = ({ socket }) => {
  const [input, setInput] = useState('');

  const sendToServer = (e) => {
    e.preventDefault();

    socket.send(JSON.stringify(input));

    setInput('');
  };
  return (
    <form method="POST" onSubmit={sendToServer}>
      <label htmlFor="input"></label>

      <input
        type="text"
        id="input"
        name="message"
        value={input}
        onChange={(e) => setInput(e.target.value)}
        maxLength={50}
      />

      <button>Отправить</button>
    </form>
  );
};
