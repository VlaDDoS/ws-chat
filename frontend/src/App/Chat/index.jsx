export const Chat = ({ messages }) => {
  return (
    <div>
      {messages.map((item, idx) => (
        <p key={idx}>{item}</p>
      ))}
    </div>
  );
};
